import React, { Component }  from 'react';
import { ScrollView, Text, TouchableOpacity, View, Dimensions, Image } from 'react-native';

export default class PictureItem extends Component {
      
    render () {
        const { picture, onPress } = this.props;
        return (
            
                
                    <TouchableOpacity style={ styles.containerStyle} onPress={ onPress }>
                        <Image 
                            style={{ flex: 1, margin: 5 }}
                            source={{ uri: picture.cropped_picture }}
                            resizeMode='cover'
                        />
                     </TouchableOpacity>
                
           
        )
    }
}

const styles = {
    containerStyle:{
        flex: 0.5,
        aspectRatio: 1
    }
};