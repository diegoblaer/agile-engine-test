export const PICTURE_LIST_FETCHING = 'picture_list_fetching';
export const PICTURE_LIST_FETCH_SUCCESS = 'picture_list_fetch_success';

export const PICTURE_FETCHING = 'picture_fetching';
export const PICTURE_FETCH_SUCCESS = 'picture_fetch_success';