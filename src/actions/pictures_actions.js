import { AsyncStorage } from 'react-native';
import _ from 'lodash';
import axios from 'axios';



import {
    PICTURE_LIST_FETCHING,
    PICTURE_LIST_FETCH_SUCCESS,
    PICTURE_FETCHING,
    PICTURE_FETCH_SUCCESS
} from './types';

let instance = axios.create({
    baseURL: 'http://195.39.233.28:8035'
});

async function renewToken(){
    console.log('renewing token...');
    let res = await instance.post('/auth', { apiKey : "23567b218376f79d9415" });
    let { data } = res;
    await AsyncStorage.setItem('authToken', String(data.token));
    return token;
};


instance.interceptors.request.use(async config => {
    var token = await AsyncStorage.getItem('authToken');
    console.log(token);
    if (token) {
      instance.defaults.headers.common['Authorization'] = 'Bearer ' + token;
      config.headers['Authorization'] = 'Bearer ' + token;
    }

    return config;
});

instance.interceptors.response.use(response => {
    return response;
  }, error => {
    const { config, response: { status } } = error;
    const originalRequest = config;
    console.log('status ' + status);
    if (status === 401) {
        renewToken();
        return instance.request(error.config);
    }
    
    return Promise.reject(error);
});

export const fetchPictures = page => async dispatch => {

    dispatch({ type: PICTURE_LIST_FETCHING });

    try{
        var res = await instance.get(`/images?page=${page}`);
        dispatch({ type: PICTURE_LIST_FETCH_SUCCESS, payload: res.data });
    }catch(error){
        console.log(error);
    }
    
};

export const fetchPicture = id => async dispatch => {

    dispatch({ type: PICTURE_FETCHING });
    console.log('fetching id ' + id);
    try{
        let res = await instance.get(`/images/${id}`);
        dispatch({ type: PICTURE_FETCH_SUCCESS, payload: res.data });

    }catch(error){
        console.log(error);
    }
    
};




