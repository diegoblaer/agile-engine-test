import React, { Component }  from 'react';
import { View, FlatList, ActivityIndicator, ScrollView, Text, TouchableOpacity } from 'react-native';
import { Button } from 'react-native-elements';
import _ from 'lodash';
import { connect } from 'react-redux';
import PictureItem from '../components/PictureItem';
import * as actions from '../actions';



 class MainScreen extends Component {
      
    static navigationOptions = {
        title: 'Gallery App'
    };

    componentDidMount(){
        this._fetchMorePictures();
    }

    _fetchMorePictures = () => {
        const { fetchPictures, page } = this.props;
        fetchPictures(page + 1);
    }

    _renderItem = ({ item }) => {
        return(
            <PictureItem 
                picture={item} 
                onPress={() => this.props.navigation.navigate('Detail', { pictureId: item.id } ) }
            />
        );
    }

    _keyExtractor = (item, index) => index.toString();

    _renderShowMoreButton = () => {
        const { hasMore, loading } = this.props;
        if(loading){
            return (<ActivityIndicator style={ styles.spinnerStyle } />);
        }else{
            return hasMore ? (<Button containerStyle={ styles.buttonStyle } onPress={() => { this._fetchMorePictures() }} title="MORE" />) : null;
        }
    }

    render () {
        const { pictureData } = this.props;
        return(
            <View style={ styles.containerStyle }>
                <FlatList
                    keyExtractor={this._keyExtractor}
                    data={ pictureData }
                    renderItem={ this._renderItem }
                    numColumns={2}
                    ListFooterComponent={ this._renderShowMoreButton }
                />
            </View>            
        );        
    }

}

const mapStateToProps = ({ pictureList }) => {
    const { loading, data, page, hasMore } = pictureList;
    const pictureData = _.map(data, (val) =>{
        return { ...val };
    });

    return { 
        loading, 
        page, 
        pictureData,
        hasMore
    };
}
  
export default connect(mapStateToProps, actions)(MainScreen);

const styles = {
    containerStyle: {
        padding: 5
    },
    buttonStyle: {
        backgroundColor: '#2f95dc',
        marginVertical: 10
    },
    spinnerStyle: {
        marginVertical: 10,
        alignSelf: 'center'
    }
};