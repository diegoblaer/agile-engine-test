import React, { Component }  from 'react';
import { View, ActivityIndicator, Text, Share, Image} from 'react-native';
import { Icon } from 'react-native-elements';
import _ from 'lodash';
import { connect } from 'react-redux';
import GestureRecognizer from 'react-native-swipe-gestures';
import * as actions from '../actions';




 class DetailScreen extends Component {
      
    static navigationOptions = {
        title: 'Details'
    };

    componentDidMount(){
        const { navigation, fetchPicture } = this.props;
        const pictureId = navigation.getParam('pictureId');
        fetchPicture(pictureId);
    }

    _onShare = async () => {
        const { full_picture } = this.props;
        try {
            const result = await Share.share({
            message:
                `Check out my photo: ${full_picture}`,
            });
        } catch (error) {
            console.log(error);
        }
    }; 

    _showPrevious = () =>{
        const { pictureList, id, fetchPicture } = this.props;
        var dataIndex = _.findIndex(pictureList.data, function(o) { return o.id == id; });
        if(dataIndex > 0 && dataIndex <= pictureList.data.length -1 ){
            fetchPicture(pictureList.data[dataIndex-1].id);
        }
    }

    _showNext = () =>{
        const { pictureList, id, fetchPicture } = this.props;
        var dataIndex = _.findIndex(pictureList.data, function(o) { return o.id == id; });
        if(dataIndex >= 0 && dataIndex < pictureList.data.length -1 ){
           fetchPicture(pictureList.data[dataIndex+1].id);
        }
    }

    render(){
        const { full_picture, loading, author, camera } = this.props;

        if(loading){
            return (
                <View style={ styles.containerStyle}>
                    <ActivityIndicator />
                </View>
            );
        }else{
            return(
                <GestureRecognizer 
                    style={ styles.containerStyle}
                    onSwipeLeft={ this._showNext.bind(this) }
                    onSwipeRight={ this._showPrevious.bind(this) }
                >

                    <View style={ styles.imgContainerStyle }>
                        <Image 
                            style={ styles.imgStyle } 
                            source={{ uri: full_picture }}
                            resizeMode='cover' 
                        />
                    </View>

                    <View style={ styles.imgOptionsStyle }>

                        <View style={ styles.infoStyle }>
                            <Text style={ styles.authorStyle }>{ author }</Text>
                            <Text style={ styles.cameraStyle }>{ camera }</Text>
                        </View>

                        <Icon 
                            name="sharealt"
                            type="antdesign"
                            color="#2f95dc"
                            size={30}
                            onPress={ this._onShare }
                        />

                    </View>

                </GestureRecognizer>
                
            );
        }
    }
}

const mapStateToProps = ({ picture, pictureList }) => {
    return { ...picture, pictureList };
}
  
export default connect(mapStateToProps, actions)(DetailScreen);

const styles = {
    containerStyle: {
        padding: 5,
        flex: 1,
        justifyContent: 'center'
    },
    imgContainerStyle: {
        flexDirection: 'row'
    },
    imgStyle: {
        flex: 1,
        height: 300
    },
    imgOptionsStyle: {
        marginVertical: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    authorStyle: {
        fontSize: 14
    },
    cameraStyle: {
        fontSize: 12
    }
};