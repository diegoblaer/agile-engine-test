import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';

import MainScreen from '../screens/MainScreen';
import DetailScreen from '../screens/DetailScreen';


/* --- Order Created Screen --- */
const mainStackNavigator = createStackNavigator({
  Main: MainScreen,
  Detail: DetailScreen
}, {
  initialRouteName: 'Main',
});




export default createAppContainer(mainStackNavigator);