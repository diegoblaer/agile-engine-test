import _ from 'lodash';

import { 
    PICTURE_FETCHING, 
    PICTURE_FETCH_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
    loading: true,
    id: '',
    author: '',
    camera: '',
    cropped_picture: '',
    full_picture: ''
};

export default function( state = INITIAL_STATE, action){
    switch(action.type){
        case PICTURE_FETCHING:
            return { ...INITIAL_STATE};
        case PICTURE_FETCH_SUCCESS:
            return { ...INITIAL_STATE, loading:false, ...action.payload };
        default: 
            return state;
    }
}