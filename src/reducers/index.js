import { combineReducers } from 'redux';
import pictureList from './picture_list_reducer';
import picture from './picture_reducer';

export default combineReducers({
    pictureList,
    picture
});