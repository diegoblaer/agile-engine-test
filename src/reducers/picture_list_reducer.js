import _ from 'lodash';

import { 
    PICTURE_LIST_FETCHING, 
    PICTURE_LIST_FETCH_SUCCESS
} from '../actions/types';

const INITIAL_STATE = {
    loading: true,
    page: 0,
    data: [],
    hasMore: false
};

export default function( state = INITIAL_STATE, action){
    switch(action.type){
        case PICTURE_LIST_FETCHING:
            return { ...state, loading: true };
        case PICTURE_LIST_FETCH_SUCCESS:
            return { ...state, 
                    loading: false, 
                    data: _.concat(state.data, action.payload.pictures), 
                    hasMore: action.payload.hasMore, 
                    page: action.payload.page 
                };
        default: 
            return state;
    }
}